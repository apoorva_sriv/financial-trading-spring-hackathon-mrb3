# Main for real time: https://medium.com/code-zen/python-generator-and-html-server-sent-events-3cdf14140e56
# Extra: https://ron.sh/creating-real-time-charts-with-flask/

from flask import Flask, Response, render_template
from yahooquery import Ticker

app = Flask(__name__)

@app.route('/<tickerSymbol>')
def index(tickerSymbol):
    return render_template('index.html', tickerSymbol=tickerSymbol)

@app.route('/data/<tickerSymbol>')
def view(tickerSymbol):
    def stream():
        while True:
            ticker = Ticker(tickerSymbol)
            quotes = ticker.quotes[0]

            try:
                price = quotes["preMarketPrice"]
                points = quotes["preMarketChange"]
                percent = quotes["preMarketChangePercent"]
            except KeyError:
                try:
                    price = quotes["postMarketPrice"]
                    points = quotes["postMarketChange"]
                    percent = quotes["postMarketChangePercent"]
                except KeyError:
                    price = quotes["regularMarketPrice"]
                    points = quotes["regularMarketChange"]
                    percent = quotes["regularMarketChangePercent"]

            plus_sign = "+" if points >= 0 else ""
            advice = ticker.financial_data[tickerSymbol]["recommendationKey"]
            output = f"{price:.2f} {plus_sign}{points:.2f} {'('}{plus_sign}{percent:.2f}{'%)'} {advice}"
            yield f"data:{output}\n\n"

    return Response(stream(), mimetype='text/event-stream')