package spring.example.trading.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import spring.example.trading.entities.Stocks;
import spring.example.trading.service.StocksService;

@RestController
@RequestMapping("/stocks")
public class StocksController {

    @Autowired
    private StocksService service;

    @RequestMapping(method=RequestMethod.GET)
    public Collection<Stocks> getStocks(){
        return service.getStocks();
    }

    @RequestMapping(method=RequestMethod.POST)
    public void addStocks(@RequestBody Stocks stocks){
        service.addStocks(stocks);
    }

    @RequestMapping(method=RequestMethod.PUT)
    public void updateStocks(){
        //todo
    }
    
}