package spring.example.trading.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Stocks {
    @Id
    private ObjectId id;
    private int StockQuantity;
    private double RequestedPrice;
    enum TradeStatus{CREATED,PENDING,CANCELLED,REJECTED,FILLED,PARTIALLY_FIILED,ERROR}

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public int getStockQuantity() {
        return StockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        StockQuantity = stockQuantity;
    }

    public double getRequestedPrice() {
        return RequestedPrice;
    }

    public void setRequestedPrice(double requestedPrice) {
        RequestedPrice = requestedPrice;
    };

    
}