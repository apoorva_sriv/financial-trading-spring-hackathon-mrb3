package com.conygre.spring.financialInstrument.service;

import java.util.Collection;
// import java.util.List;
import java.util.Optional;

import com.conygre.spring.financialInstrument.entities.Trade;
import com.conygre.spring.financialInstrument.entities.TradeState;
import com.conygre.spring.financialInstrument.repo.TradeRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TradeServiceImpl implements TradeService {
    @Autowired
    private TradeRepository repo;

    @Override
    public Collection<Trade> getTrade() {
        return repo.findAll();
    }

    @Override
    public void addTrade(Trade trade) {
        repo.insert(trade);
    }

    @Override
    public void deleteTrade(Trade trade) {
        repo.delete(trade);
    }

    @Override
    public void deleteTradeById(ObjectId id) {
        repo.deleteById(id);

    }

    @Override
    public Optional<Trade> getTradeById(ObjectId id) {
        return repo.findById(id);
    }


    @Override
    public Collection<Trade> getTradeByTicker(String ticker) {
        return repo.findByTicker(ticker);
    }

    @Override
    public Collection<Trade> getTradeByState(TradeState state) {
        return repo.findByState(state);
    }


    @Override
    public void updateTrade(ObjectId id, double price) {


        Optional<Trade> trade = repo.findById(id);
        if (trade != null) {
            Trade trade_old = trade.get();
            trade_old.setPrice(price);
            repo.save(trade_old);
        }

    //     trade_old.setCreated(trade_new.getCreated());
    //     trade_old.setState(trade_new.getState());
    //     trade_old.setType(trade_new.getType());
    //     trade_old.setTicker(trade_new.getTicker());

    //     trade_old.setQuantity(trade_new.getQuantity());
    //     trade_old.setPrice(trade_new.getPrice());
    //     repo.save(trade_old);
    }

    @Override
    public void deleteAll() {
        repo.deleteAll();

    }

}