package com.conygre.spring.financialInstrument.service;

import java.util.Collection;
// import java.util.List;

import com.conygre.spring.financialInstrument.entities.Trade;
import com.conygre.spring.financialInstrument.entities.TradeState;

import org.bson.types.ObjectId;
import java.util.Optional;
public interface TradeService {
    Collection<Trade> getTrade();
    void addTrade(Trade trade);
    void updateTrade(ObjectId id,double price);
    void deleteTrade(Trade trade);
    void deleteTradeById(ObjectId id);
    void deleteAll();

    Optional<Trade> getTradeById(ObjectId id);

    Collection<Trade> getTradeByTicker(String Ticker);
    Collection<Trade> getTradeByState(TradeState state);


}

