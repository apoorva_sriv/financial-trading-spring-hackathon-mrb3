package com.conygre.spring.financialInstrument.repo;

import java.util.Collection;
// import java.util.List;

import com.conygre.spring.financialInstrument.entities.Trade;
import com.conygre.spring.financialInstrument.entities.TradeState;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import org.springframework.stereotype.Repository;
@Repository
public interface TradeRepository extends MongoRepository<Trade,ObjectId>{

	public Collection<Trade> findByTicker(String ticker);

	public Collection<Trade> findByState(TradeState state);


}