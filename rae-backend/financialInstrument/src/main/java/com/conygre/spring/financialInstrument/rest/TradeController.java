package com.conygre.spring.financialInstrument.rest;

import com.conygre.spring.financialInstrument.entities.Trade;
import com.conygre.spring.financialInstrument.entities.TradeState;
import com.conygre.spring.financialInstrument.service.TradeService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.*;

import java.util.Collection;
// import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RequestMethod;



@RestController
@CrossOrigin
public class TradeController {

    @Autowired
    private TradeService service;

	
	@RequestMapping(method = RequestMethod.GET,value = "/trade")
    public Iterable<Trade> findAll() {
		return service.getTrade();
    }



    @RequestMapping(method = RequestMethod.POST,value = "/trade")
	public void addTd(@RequestBody Trade trade) {
		service.addTrade(trade);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/trade/{id}")
	public Optional<Trade> getTdById(@PathVariable("id") String id) {
		return service.getTradeById(new ObjectId(id));
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/trade/{id}")
	public void deleteTd(@PathVariable("id") String id) {
		service.deleteTradeById(new ObjectId(""+id));
	}

	@RequestMapping(method = RequestMethod.DELETE,value = "/trade")
	public void deleteTd() {
		service.deleteAll();

	}
	
	@RequestMapping(method = RequestMethod.GET,value = "/trade/ticker/{ticker}")
	@ResponseBody
    public Collection<Trade> findByTicker(@PathVariable("ticker") String ticker){
       return service.getTradeByTicker(ticker);
	}

	@RequestMapping(method = RequestMethod.GET,value = "/trade/state/{state}")
	@ResponseBody
    public Collection<Trade> findByState(@PathVariable("state") TradeState state){
       return service.getTradeByState(state);
	}

	// @RequestMapping(method = RequestMethod.PUT,value = "/trade/update/{id}")
	// // @ResponseBody
    // public void updateTd(@PathVariable("id") String id,@RequestBody Trade trade_new){
	// 	// Optional<Trade> trade = service.getTradeById();
	// 	// Trade trade_old=trade.get();

    //     service.updateTrade(new ObjectId(""+id),trade_new);
	// }


	@RequestMapping( method = RequestMethod.PATCH,path = "/trade/update/{id}")
	
    public void updateTd(@PathVariable("id") String id, @RequestBody int price) {
        service.updateTrade(new ObjectId(""+id), price);
   }

   
}