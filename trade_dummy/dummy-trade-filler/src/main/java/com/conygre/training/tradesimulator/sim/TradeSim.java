package com.conygre.training.tradesimulator.sim;

import java.util.List;

import com.conygre.training.tradesimulator.dao.TradeMongoDao;
import com.conygre.training.tradesimulator.model.Stocks;
import com.conygre.training.tradesimulator.model.TradeStatus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class TradeSim {
    private static final Logger LOG = LoggerFactory.getLogger(TradeSim.class);

    @Autowired
    private TradeMongoDao tradeDao;

    @Transactional
    public List<Stocks> findTradesForProcessing(){
        List<Stocks> foundTrades = tradeDao.findByStatus(TradeStatus.CREATED);

        for(Stocks thisTrade: foundTrades) {
            thisTrade.setStatus(TradeStatus.PROCESSING);
            tradeDao.save(thisTrade);
        }

        return foundTrades;
    }

    @Transactional
    public List<Stocks> findTradesForFilling(){
        List<Stocks> foundTrades = tradeDao.findByStatus(TradeStatus.PROCESSING);

        for(Stocks thisTrade: foundTrades) {
            if((int) (Math.random()*10) > 8) {
                thisTrade.setStatus(TradeStatus.REJECTED);
            }
            else {
                thisTrade.setStatus(TradeStatus.FILLED);
            }
            tradeDao.save(thisTrade);
        }

        return foundTrades;
    }

    @Scheduled(fixedRateString = "${scheduleRateMs:10000}")
    public void runSim() {
        LOG.debug("Main loop running!");

        int tradesForFilling = findTradesForFilling().size();
        LOG.debug("Found " + tradesForFilling + " trades to be filled/rejected");

        int tradesForProcessing = findTradesForProcessing().size();
        LOG.debug("Found " + tradesForProcessing + " trades to be processed");

    }
}
