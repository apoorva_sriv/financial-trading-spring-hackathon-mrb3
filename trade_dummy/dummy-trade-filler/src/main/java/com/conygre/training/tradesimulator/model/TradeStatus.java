package com.conygre.training.tradesimulator.model;

public enum TradeStatus {
    CREATED("CREATED"),
    PROCESSING("PROCESSSING"),
    FILLED("FILLED"),
    REJECTED("REJECTED");

    private String status;

    private TradeStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    } 
}

    