package spring.example.trading.repo;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import spring.example.trading.entities.Stocks;

public interface StocksRepo extends MongoRepository<Stocks,ObjectId> 

{

    List<Stocks> findByTickerContaining(String ticker);

    
}