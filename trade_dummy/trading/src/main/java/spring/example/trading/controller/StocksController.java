package spring.example.trading.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import spring.example.trading.entities.Stocks;
import spring.example.trading.repo.StocksRepo;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("")
public class StocksController {

  @Autowired
  StocksRepo stocksRepository;

  @GetMapping("/stocks")
  public ResponseEntity<List<Stocks>> getAllstocks(@RequestParam(required = false) String ticker) {
    try {
      List<Stocks> stocks = new ArrayList<Stocks>();

      if (ticker == null)
        stocksRepository.findAll().forEach(stocks::add);
      else
        stocksRepository.findByTickerContaining(ticker).forEach(stocks::add);

      if (stocks.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(stocks, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/stocks/{id}")
  public ResponseEntity<Stocks> getstocksById(@PathVariable("id") ObjectId id) {
    Optional<Stocks> stocksData = stocksRepository.findById(id);

    if (stocksData.isPresent()) {
      return new ResponseEntity<>(stocksData.get(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping("/stocks")
  public ResponseEntity<Stocks> createstocks(@RequestBody Stocks stocks) {
    try {
      Stocks _stocks = stocksRepository.save(new Stocks(stocks.getTicker(), stocks.getStockQuantity(), stocks.getRequestedPrice()));
      return new ResponseEntity<>(_stocks, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/stocks/{id}")
  public ResponseEntity<Stocks> updatestocks(@PathVariable("id") ObjectId id, @RequestBody Stocks stocks) {
    Optional<Stocks> stocksData = stocksRepository.findById(id);

    if (stocksData.isPresent()) {
      Stocks _stocks = stocksData.get();
      _stocks.setTicker(stocks.getTicker());
      _stocks.setRequestedPrice(stocks.getRequestedPrice());
      _stocks.setStockQuantity(stocks.getStockQuantity());
      return new ResponseEntity<>(stocksRepository.save(_stocks), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/stocks/{id}")
  public ResponseEntity<HttpStatus> deletestocks(@PathVariable("id") ObjectId id) {
    try {
      stocksRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @DeleteMapping("/stocks")
  public ResponseEntity<HttpStatus> deleteAllstocks() {
    try {
      stocksRepository.deleteAll();
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  

}
