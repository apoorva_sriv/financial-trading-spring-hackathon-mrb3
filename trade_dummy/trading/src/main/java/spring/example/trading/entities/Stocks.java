package spring.example.trading.entities;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Stocks {
    @Id
    private ObjectId id;
    private Date created = new Date(System.currentTimeMillis());
    private String ticker;
    private int stockQuantity;
    private double requestedPrice;
    private TradeStatus status= TradeStatus.CREATED;;

    private TradeType type = TradeType.BUY;


    public Stocks(){
    }

    public String getHexString(){
        return id.toHexString();
    }
    public Stocks(String ticker, int stockQuantity, double requestedPrice){
        this.ticker= ticker;
        this.stockQuantity = stockQuantity;
        this.requestedPrice = requestedPrice;
    }
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
       this.stockQuantity = stockQuantity;
    }

    public double getRequestedPrice() {
        return requestedPrice;
    }

    public void setRequestedPrice(double requestedPrice) {
        this.requestedPrice = requestedPrice;
    }

	public String getTicker() {
		return ticker;
	};

    public void setTicker(String t) {
        this.ticker = t;
	}

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public TradeStatus getStatus() {
        return status;
    }

    public void setStatus(TradeStatus status) {
        this.status = status;
    }

    public TradeType getType() {
        return type;
    }

    public void setType(TradeType type) {
        this.type = type;
    };
    
}